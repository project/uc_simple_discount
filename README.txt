This module aims to provide a simple approach to applying discounts in Ubercart.

It's configurable with rules so you can apply any percentage discount based on
any rules conditions available. Discounts are applied on a per-product basis,
so any VAT or price alterations done on the product will still be accounted, so
it will work with a mix tax/non-taxed products.

<b>How to use this module</b>

1. Install and enable as usual.
2. Create a new <a href="http://drupal.org/project/rules">rule</a> for the Cart
event "Cart's product is being altered.".
3. Add the conditions you need, this module adds a new one called "Check a
cart's subtotal" that you can use to check cart subtotal.
4. Add the action "Apply a discount to the cart product" and select the discount
amount you want to use.

There's also a settings page at admin/store/settings/uc_simple_discount that
allows you to:
- Show discount information on its own on cart and checkout pages (the name of
the rule configured above will be used).
- Extra discount definitions that can be used in admin created orders.

Sponsored by <a href="http://www.infomagnet.com">Infomagnet</a>.
